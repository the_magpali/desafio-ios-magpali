//
//  APIClient.swift
//  Desafio iOS Magpali
//
//  Created by Victor Robertson on 17/07/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import Moya
import RxSwift
import RxCocoa
import ObjectMapper
import Moya_ObjectMapper

let DefaultProvider = Provider<Target>()

class APIClient: NSObject {

    static func getRepositories(language: String? = "Java", sort: String? = "star", page: Int? = 1) -> Observable<[Repository]> {
        return DefaultProvider.request(Target.getRepositories(language: language, sort: sort, page: page))
            .processResponse()
            .mapDataArray(Repository.self)
    }
    
    static func getPullRequests(owner: String, repository: String, page: Int? = 1) -> Observable<[PullRequest]> {
        return DefaultProvider.request(Target.getPullRequests(owner: owner, repository: repository, page: page))
            .processResponse()
            .mapArray(PullRequest.self)
    }
    
}
