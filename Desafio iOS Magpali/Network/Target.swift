//
//  Target.swift
//  Desafio iOS Magpali
//
//  Created by Victor Robertson on 17/07/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import Moya
import RxSwift

enum Target {
    case getRepositories(language: String?, sort: String?, page: Int?)
    case getPullRequests(owner: String, repository: String, page: Int?)
}

extension Target: TargetType {
    
    var baseURL: URL { return URL(string: "https://api.github.com/")! }
    
    var path: String {
        switch self {
            case .getRepositories:
                return "search/repositories"
            case .getPullRequests(let owner, let repository, _):
                return "repos/\(owner)/\(repository)/pulls"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getRepositories,
             .getPullRequests:
            return .get
        }
    }
    
    var parameters: [String: Any]? {
        var parameters: [String: Any] = [:]
        switch self {
        case .getRepositories(let language, let sort, let page):
            if let language = language {
                parameters["q"] = "language:\(language)" as Any
            }
            if let sort = sort {
                parameters["sort"] = sort as Any
            }
            if let page = page {
                parameters["page"] = page as Any
            }
        case .getPullRequests(_, _, let page):
            if let page = page {
                parameters["page"] = page as Any
            }
        }
        return parameters
    }
    
    var parameterEncoding: ParameterEncoding {
        return URLEncoding.queryString
    }
    
    var task: Task {
        return Task.request
    }
    
    var sampleData: Data {
        return Data()
    }
    
}
