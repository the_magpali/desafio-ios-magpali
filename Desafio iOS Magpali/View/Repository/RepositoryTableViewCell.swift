//
//  RepositoryTableViewCell.swift
//  Desafio iOS Magpali
//
//  Created by Victor Robertson on 18/07/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import UIKit
import Kingfisher

class RepositoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var ownerNameLabel: UILabel!
    @IBOutlet weak var forksImageVIew: UIImageView!
    @IBOutlet weak var forksLabel: UILabel!
    @IBOutlet weak var starsImageView: UIImageView!
    @IBOutlet weak var starsLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        applyLayout()
    }
    
    private func applyLayout() {
        nameLabel.textColor = UIColor.lightBlue
        nameLabel.font = UIFont.systemFont(ofSize: 20)
        ownerNameLabel.textColor = UIColor.lightBlue
        descriptionLabel.numberOfLines = 2
        avatarImageView.layer.cornerRadius = avatarImageView.frame.size.width/2
        avatarImageView.contentMode = .scaleToFill
        avatarImageView.clipsToBounds = true
        forksImageVIew.image = R.image.fork()?.withRenderingMode(.alwaysTemplate)
        forksImageVIew.contentMode = .scaleToFill
        forksImageVIew.tintColor = UIColor.lightOrange
        forksLabel.textColor = UIColor.lightOrange
        forksLabel.font = UIFont.systemFont(ofSize: 18)
        starsImageView.image = R.image.star()?.withRenderingMode(.alwaysTemplate)
        starsImageView.contentMode = .scaleToFill
        starsImageView.tintColor = UIColor.lightOrange
        starsLabel.textColor = UIColor.lightOrange
        starsLabel.font = UIFont.systemFont(ofSize: 18)
    }

    func populate(with repository: Repository) {
        if let avatar = repository.owner?.avatar, let url = URL(string: avatar) {
            avatarImageView.kf.setImage(with: url, placeholder: R.image.avatar())
        }
        nameLabel.text = repository.name
        descriptionLabel.text = repository.descriptionText
        forksLabel.text = "\(repository.forks ?? 0)"
        starsLabel.text = "\(repository.stargazers ?? 0)"
        ownerNameLabel.text = repository.owner?.login
        
    }
    
}
