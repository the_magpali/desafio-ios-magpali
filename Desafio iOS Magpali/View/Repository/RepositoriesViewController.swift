//
//  RepositoriesViewController.swift
//  Desafio iOS Magpali
//
//  Created by Victor Robertson on 19/07/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import UIKit
import RxSwift

class RepositoriesViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel = RepositoriesViewModel()
    var disposeBag = DisposeBag()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.cyan
        navigationController?.navigationBar.isHidden = false
        title = "Github Java"
        configTableView()
        bind()
    }
    
    func configTableView() {
        tableView.register(R.nib.repositoryTableViewCell(), forCellReuseIdentifier: R.reuseIdentifier.respositoryTableViewCell.identifier)
        tableView.estimatedRowHeight = 100
        tableView.tableFooterView = UIView()
        tableView.alwaysBounceVertical = false
    }
    
    func bind() {
        viewModel.getRepositories()
            .bind(to: tableView.rx.items(cellIdentifier: R.reuseIdentifier.respositoryTableViewCell.identifier, cellType: RepositoryTableViewCell.self))
                { (row, element, cell) in
                    cell.populate(with: element)
                }
            .addDisposableTo(disposeBag)
        
        tableView.rx.willDisplayCell.asObservable().subscribe(onNext: { [weak self] (cell: UITableViewCell, indexPath: IndexPath) in
            if indexPath.row == self?.viewModel.getInfiniteScrollTrigger() {
                self?.viewModel.getNextPage()
            }
        }).addDisposableTo(disposeBag)
        
        tableView.rx.itemSelected.asObservable().subscribe(onNext: { [weak self] (indexPath) in
            print(indexPath.row)
            let viewController = PullRequestsViewController(nibName: R.nib.pullRequestsViewController.name, bundle: nil)
            viewController.viewModel.repository.value = self?.viewModel.repositories.value[indexPath.row]
            self?.navigationController?.pushViewController(viewController, animated: true)
        }).addDisposableTo(disposeBag)

    }

}
