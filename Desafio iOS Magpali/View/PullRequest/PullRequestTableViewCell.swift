//
//  PullRequestTableViewCell.swift
//  Desafio iOS Magpali
//
//  Created by Victor Robertson on 20/07/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import UIKit

class PullRequestTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        applyLayout()
    }
    
    private func applyLayout() {
        titleLabel.textColor = UIColor.lightBlue
        titleLabel.font = UIFont.systemFont(ofSize: 20)
        descriptionLabel.numberOfLines = 2
        avatarImageView.layer.cornerRadius = avatarImageView.frame.size.width/2
        avatarImageView.contentMode = .scaleToFill
        avatarImageView.clipsToBounds = true
        nameLabel.textColor = UIColor.lightBlue
        dateLabel.font = UIFont.systemFont(ofSize: 12)
        dateLabel.textColor = UIColor.lightGray
    }
    
    func populate(with pullRequest: PullRequest) {
        if let avatar = pullRequest.user?.avatar, let url = URL(string: avatar) {
            avatarImageView.kf.setImage(with: url, placeholder: R.image.avatar())
        }
        titleLabel.text = pullRequest.title
        descriptionLabel.text = pullRequest.body
        nameLabel.text = pullRequest.user?.login
        dateLabel.text = pullRequest.date?.dayString()
    }
}
