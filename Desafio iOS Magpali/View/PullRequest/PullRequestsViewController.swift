//
//  PullRequestsViewController.swift
//  Desafio iOS Magpali
//
//  Created by Victor Robertson on 20/07/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import UIKit
import RxSwift

class PullRequestsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    let viewModel = PullRequestsViewModel()
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.cyan
        navigationController?.navigationBar.isHidden = false
        configTableView()
        bind()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.getPullRequests()
    }
    
    func configTableView() {
        tableView.register(R.nib.pullRequestTableViewCell(), forCellReuseIdentifier: R.reuseIdentifier.pullRequestTableViewCell.identifier)
        tableView.estimatedRowHeight = 100
        tableView.tableFooterView = UIView()
        tableView.alwaysBounceVertical = false
    }
    
    func bind() {
        viewModel.observablePullRequests()
            .bind(to: tableView.rx.items(cellIdentifier: R.reuseIdentifier.pullRequestTableViewCell.identifier, cellType: PullRequestTableViewCell.self))
            { (row, element, cell) in
                cell.populate(with: element)
            }
            .addDisposableTo(disposeBag)

        tableView.rx.willDisplayCell.asObservable().subscribe(onNext: { [weak self] (cell: UITableViewCell, indexPath: IndexPath) in
            if indexPath.row == self?.viewModel.getInfiniteScrollTrigger() {
                self?.viewModel.getNextPage()
            }
        }).addDisposableTo(disposeBag)
        
        viewModel.observableTitle().subscribe(onNext: { [weak self] (title) in
            self?.title = title
        }).addDisposableTo(disposeBag)
        
        tableView.rx.itemSelected.asObservable().subscribe(onNext: { [weak self] (indexPath) in
            if let urlString = self?.viewModel.pullRequests.value[indexPath.row].url, let url = URL(string: urlString) {
                UIApplication.shared.openURL(url)
            }
        }).addDisposableTo(disposeBag)
    }
}
