//
//  Helper.swift
//  Desafio iOS Magpali
//
//  Created by Victor Robertson on 17/07/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import UIKit
import Moya
import RxSwift
import ObjectMapper

class Helper {
    class func JSONResponseDataFormatter(_ data: Data) -> Data {
        do {
            let dataAsJSON = try JSONSerialization.jsonObject(with: data)
            let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
            return prettyData
        } catch {
            return data // fallback to original data if it can't be serialized.
        }
    }
    
    class func reversedPrint(seperator: String, terminator: String, items: Any...) {
        for item in items {
            print(item)
        }
    }
}

public extension String {
    func dayString() -> String {
        let formater = DateFormatter()
        formater.locale = Locale(identifier: "en_US_POSIX")
        formater.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        guard let date = formater.date(from: self) else { return "Invalid Date" }
        formater.dateFormat = "MM-dd-yyyy"
        return formater.string(from: date)
    }
}

public extension UIColor {
    public class var lightOrange: UIColor { return UIColor(red: 255/255, green: 153/255, blue: 51/255, alpha: 1.0) }
    public class var lightBlue: UIColor { return UIColor(red: 51/255, green: 102/255, blue: 187/255, alpha: 1.0) }
}

public extension Response {
    /// Maps data received from the signal into an array of objects which implement the Mappable
    /// protocol.
    /// If the conversion fails, the signal errors.
    public func mapDataArray<T: BaseMappable>(_ type: T.Type, context: MapContext? = nil) throws -> [T] {
        guard let json = try mapJSON() as? [String: Any], let array = json["items"] as? [[String : Any]] else {
            throw MoyaError.jsonMapping(self)
        }
        return Mapper<T>(context: context).mapArray(JSONArray: array)
    }
}

public extension ObservableType where E == Response {
    /// Maps data received from the signal into an array of objects
    /// which implement the Mappable protocol and returns the result back
    /// If the conversion fails, the signal errors.
    public func mapDataArray<T: BaseMappable>(_ type: T.Type, context: MapContext? = nil) -> Observable<[T]> {
        return flatMap { response -> Observable<[T]> in
            return Observable.just(try response.mapDataArray(T.self, context: context))
        }
    }
}
