//
//  Owner.swift
//  Desafio iOS Magpali
//
//  Created by Victor Robertson on 18/07/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import UIKit
import ObjectMapper

class Owner: Mappable {
    
    var id: Int?
    var login: String?
    var avatar: String?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id <- map["id"]
        login <- map["login"]
        avatar <- map["avatar_url"]
    }
}
