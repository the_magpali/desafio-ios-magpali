//
//  PullRequest.swift
//  Desafio iOS Magpali
//
//  Created by Victor Robertson on 20/07/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import UIKit
import ObjectMapper

class PullRequest: Mappable {
    
    var title: String?
    var body: String?
    var date: String?
    var url: String?
    var user: Owner?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        title <- map["title"]
        body <- map["body"]
        date <- map["created_at"]
        url <- map["html_url"]
        user <- map["user"]
    }

}
