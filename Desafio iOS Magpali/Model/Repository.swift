//
//  Repository.swift
//  Desafio iOS Magpali
//
//  Created by Victor Robertson on 18/07/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import UIKit
import ObjectMapper

class Repository: Mappable {
    
    var id: Int?
    var name: String?
    var descriptionText: String?
    var owner: Owner?
    var forks: Int?
    var stargazers: Int?
    
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        descriptionText <- map["description"]
        owner <- map["owner"]
        forks <- map["forks"]
        stargazers <- map["stargazers_count"]
    }

}
