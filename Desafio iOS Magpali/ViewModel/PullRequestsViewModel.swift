//
//  PullRequestsViewModel.swift
//  Desafio iOS Magpali
//
//  Created by Victor Robertson on 20/07/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import UIKit
import RxSwift

class PullRequestsViewModel {
    
    var disposeBag = DisposeBag()
    let loading = Variable<Bool>(false)
    
    let repository = Variable<Repository?>(nil)
    let pullRequests = Variable<[PullRequest]>([])
    let title = Variable<String?>(nil)
    
    var currentPage = 1
    
    func getPullRequests() {
        if let repo = repository.value?.name, let owner = repository.value?.owner?.login {
            APIClient.getPullRequests(owner: owner, repository: repo).subscribe(onNext: { [weak self] (prs) in
                self?.pullRequests.value = prs
            }).addDisposableTo(disposeBag)
        }
    }
    
    func getNextPage() {
        currentPage += 1
        if let repo = repository.value?.name, let owner = repository.value?.owner?.login {
            APIClient.getPullRequests(owner: owner, repository: repo, page: currentPage).subscribe(onNext: { [weak self] (prs) in
                self?.pullRequests.value.append(contentsOf: prs)
            }).addDisposableTo(disposeBag)
        }
    }
    
    func observablePullRequests() -> Observable<[PullRequest]> {
        return pullRequests.asObservable()
    }
    
    func observableTitle() -> Observable<String?> {
        return repository.asObservable().map({ (repository) -> String? in
            return repository?.name
        })
    }
    
    func getInfiniteScrollTrigger() -> Int {
        if pullRequests.value.count > 6 {
            return pullRequests.value.count - 5
        } else {
            return pullRequests.value.count
        }
    }
    
}
