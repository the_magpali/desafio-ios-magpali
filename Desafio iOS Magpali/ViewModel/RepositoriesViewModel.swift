//
//  RepositoriesViewModel.swift
//  Desafio iOS Magpali
//
//  Created by Victor Robertson on 19/07/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import UIKit
import RxSwift

class RepositoriesViewModel {

    var disposeBag = DisposeBag()
    let loading = Variable<Bool>(false)
    let error = Variable<NSError?>(nil)
    
    let repositories = Variable<[Repository]>([])
    var currentPage = 1
    
    init() {
        requestRepositories()
    }
    
    func requestRepositories() {
        loading.value = true
        APIClient.getRepositories(page: currentPage).subscribe(onNext: { [weak self] (repositories) in
            self?.repositories.value = repositories
            self?.loading.value = false
        }, onError: { [weak self] (error) in
            self?.error.value = error as NSError
            self?.loading.value = false
        }).addDisposableTo(disposeBag)
    }
    
    func getNextPage() {
        currentPage += 1
        loading.value = true
        APIClient.getRepositories(page: currentPage).subscribe(onNext: { [weak self] (repositories) in
            self?.repositories.value.append(contentsOf: repositories)
            self?.loading.value = false
            }, onError: { [weak self] (error) in
                self?.error.value = error as NSError
                self?.loading.value = false
        }).addDisposableTo(disposeBag)
    }
    
    func refresh() {
        currentPage = 1
        requestRepositories()
    }
    
    func getRepositories() -> Observable<[Repository]>  {
        return self.repositories.asObservable()
    }
    
    func getInfiniteScrollTrigger() -> Int {
        if repositories.value.count > 6 {
            return repositories.value.count - 5
        } else {
            return repositories.value.count
        }
    }
    
}
